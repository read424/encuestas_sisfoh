<!DOCTYPE html>
<html lang="es" ng-app="EncuestasAdmin">
   <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <title>Sistema de Gestión</title>
      <meta content="Admin Dashboard" name="description" />
      <meta content="Themesbrand" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="assets/images/favicon.ico">
      <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="assets/css/toastr.min.css" rel="stylesheet" type="text/css">
      <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="assets/css/style.css" rel="stylesheet" type="text/css">
   </head>
    <body class="fixed-left">
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>
        <div ui-view></div>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/tether.min.js"></script> 
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/modernizr.min.js"></script> 
        <script src="assets/js/jquery.slimscroll.js"></script> 
        <script src="assets/js/waves.js"></script> 
        <script src="assets/js/jquery.nicescroll.js"></script> 
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="node_modules/angular/angular.min.js"></script>
        <script src="node_modules/@uirouter/angularjs/release/angular-ui-router.min.js"></script>
        <script src="assets/js/app.js"></script>
        <script src="app/sides/sides.module.js"></script>
        <script src="app/components/components.module.js"></script>
        <script src="app/components/pageTop/pageTop.directive.js"></script>
    </body>   
</html>