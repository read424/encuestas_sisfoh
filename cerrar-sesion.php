<?php
  session_start();
   
  // Elimina la variable email en sesión.
  unset($_SESSION['email']);
 
  // Elimina la sesion.
  session_destroy();
   
  // Redirecciona a la página de login.
  header("HTTP/1.1 302 Moved Temporarily");
  echo "<META HTTP-EQUIV=Refresh CONTENT=1;URL=index.php>";
  //header("Location: index.php");
?>