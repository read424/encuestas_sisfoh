<?php
  require_once('menu.php');

  $consulta = "SELECT * FROM usuario where activo='1'";
  $resultado = $conexion -> query($consulta);

  $consulta2 = "SELECT * FROM usuario where activo='0'";
  $resultado2 = $conexion -> query($consulta2);
?>
<div class="wrapper">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="page-title-box">
               <div class="btn-group pull-right">
                  <ol class="breadcrumb hide-phone p-0 m-0">
                     <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                     <li class="breadcrumb-item active">Usuarios</li>
                     <li class="breadcrumb-item active">
                     		<a  name="btneliminar" class="btn btn-primary btn-sm get-code" href="usuario_insertar.php" style="color: white;"><i class="fa fa-plus-circle" title="Nuevo Usuario" data-toggle="tooltip"> Nuevo Usuario</i></a>
                 		</li>
                  </ol>
               </div>
               <h4 class="page-title">Listado de Usuarios</h4>
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-lg-12">
            <div class="card m-b-20">
               <div class="card-block">
                  <!-- <h4 class="mt-0 header-title">Default Tabs</h4>-->
                  <ul class="nav nav-tabs" role="tablist">
                     <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Usuarios Activos</a></li>
                     <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Usuarios Inactivos</a></li>
                  </ul>
                  <div class="tab-content">
                     <div class="tab-pane active p-3" id="home" role="tabpanel">
                        <table id="datatable-buttons" class="table nowrap table-striped table-bordered table-hover table-sm compact letraTable" cellspacing="0" width="100%">
                          <thead>
                            <tr class="fondoTabla">
                              <th>Nombre Usuario</th>
                              <th>Apellido Usuario</th>
                              <th>Alias Usuario</th>
                              <th>Telefono</th>
                              <th>Rol Usuario</th>
                              <th>Acciones</th>
                            </tr>
                          </thead>
                          <tfoot>
                            <tr class="fondoTabla">
                              <th>Nombre Usuario</th>
                              <th>Apellido Usuario</th>
                              <th>Alias Usuario</th>
                              <th>Telefono</th>
                              <th>Rol Usuario</th>
                              <th>Acciones</th>
                            </tr>
                          </tfoot>
                          <tbody>
                            <?php
                            if ($resultado->num_rows > 0) //si la variable tiene al menos 1 fila entonces seguimos con el codigo
                            {
                              while ($resulusu=$resultado->fetch_array(MYSQLI_BOTH)) {
                            ?>
                              <tr>
                                <td>
                                  <?php echo $resulusu['nombre_usuar'] ;   ?>
                                </td>
                                <td>
                                  <?php echo $resulusu['apellido_usuar'] ;   ?>
                                </td>
                                <td>
                                  <?php echo $resulusu['alias_usuar'] ;   ?>
                                </td>
                                <td>
                                  <?php echo $resulusu['telefono_usuar'] ;   ?>
                                </td>
                                <td>
                                  <?php echo $resulusu['rol_usuar'] ;   ?>
                                </td>
                                <td align="center">
                                   
                                  <a onclick="return confirm('Seguro que desea Eliminar');" name="btneliminar" class="btn btn-danger btn-sm get-code" href="usuario_eliminar.php?idcat=<?php echo $resulusu['idusuario'];?>"><i class="fa fa-trash-o" title="Eliminar" data-toggle="tooltip"></i></a>            
                                  <a  onclick="return confirm('Seguro que desea Modificar');" class="btn btn-info btn-sm get-code" href="usuario_modificar.php?idcat=<?php echo $resulusu['idusuario'];?>"><i class="fa fa-pencil-square-o" title="Modificar" data-toggle="tooltip"></i></a>
                                </td>
                              </tr>

                            <?php
                              }
                            }
                            
                            ?>
                           </tbody>
                        </table>
                     	</div>
                     	<div class="tab-pane p-3" id="profile" role="tabpanel">
                       	<table id="datatable-buttons2" class="table nowrap table-striped table-bordered table-hover table-sm compact letraTable" cellspacing="0" width="100%">
                          <thead>
                            <tr class="fondoTabla">
                              <th>Nombre Usuario</th>
                              <th>Apellido Usuario</th>
                              <th>Alias Usuario</th>
                              <th>Telefono</th>
                              <th>Rol Usuario</th>
                              <th>Acciones</th>
                            </tr>
                          </thead>
                          <tfoot>
                            <tr class="fondoTabla">
                              <th>Nombre Usuario</th>
                              <th>Apellido Usuario</th>
                              <th>Alias Usuario</th>
                              <th>Telefono</th>
                              <th>Rol Usuario</th>
                              <th>Acciones</th>
                            </tr>
                          </tfoot>
                          <tbody>
                            <?php
                            if ($resultado2->num_rows > 0) //si la variable tiene al menos 1 fila entonces seguimos con el codigo
                            {
                              while ($resulusu2=$resultado2->fetch_array(MYSQLI_BOTH)) {
                            ?>

                              <tr>
                                <td>
                                  <?php echo $resulusu2['nombre_usuar'] ;   ?>
                                </td>
                                <td>
                                  <?php echo $resulusu2['apellido_usuar'] ;   ?>
                                </td>
                                <td>
                                  <?php echo $resulusu2['alias_usuar'] ;   ?>
                                </td>
                                <td>
                                  <?php echo $resulusu2['telefono_usuar'] ;   ?>
                                </td>
                                <td>
                                  <?php echo $resulusu2['rol_usuar'] ;   ?>
                                </td>
                                <td align="center">
                                  <a  onclick="return confirm('Seguro que desea Modificar');" class="btn btn-info btn-sm get-code" href="usuario_modificar.php?idcat=<?php echo $resulusu2['idusuario'];?>"><i class="fa fa-pencil-square-o" title="Modificar" data-toggle="tooltip"></i></a>
                                </td>
                              </tr>
                            <?php
                                }
                              }
                            ?>
                          </tbody>
                      	</table>
                     	</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
   require_once('pie.php');
?>