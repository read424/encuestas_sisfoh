(function(){
    'use strict';
    angular.module('EncuestasAdmin.components',[])
        .directive('baSidebar', [function(){
            return {
                restrict: 'E',
                templateUrl: 'app/components/menu/ba-sidebar.html'
            }
        }])
})()