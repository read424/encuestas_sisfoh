(function(){
    'use strict';
    angular.module('EncuestasAdmin.components')
        .directive('pageTop', [function(){
            return {
                restrict: 'E',
                templateUrl: 'app/components/pageTop/pageTop.html'
            }
        }])
        .directive('baSidebar', [function(){
            return {
                restrict: 'E',
                templateUrl: 'app/components/menu/ba-sidebar.html'
            }
        }])
})()