-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-09-2018 a las 15:51:20
-- Versión del servidor: 5.5.25a
-- Versión de PHP: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sisconthogar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agua`
--

CREATE TABLE IF NOT EXISTS `agua` (
  `id_ag` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_ag` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_ag`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `agua`
--

INSERT INTO `agua` (`id_ag`, `desc_ag`) VALUES
(1, '¿Red publica dentro de la vivienda?'),
(2, '¿Red publica fuera de la vivienda, pero dentro del edificio?'),
(3, '¿Pilon de uso publico?'),
(4, '¿Camion cisterna u otro uso similar?'),
(5, '¿Pozo?'),
(6, '¿Rio,acequia,manantial o similar?'),
(7, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumbrado`
--

CREATE TABLE IF NOT EXISTS `alumbrado` (
  `id_alum` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_alum` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_alum`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `alumbrado`
--

INSERT INTO `alumbrado` (`id_alum`, `desc_alum`) VALUES
(1, '¿Electricidad?'),
(2, '¿Kerosene?'),
(3, '¿Petroleo?'),
(4, '¿Gas?'),
(5, '¿Vela?'),
(6, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `artefacto`
--

CREATE TABLE IF NOT EXISTS `artefacto` (
  `id_art` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_art` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_art`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `artefacto`
--

INSERT INTO `artefacto` (`id_art`, `desc_art`) VALUES
(1, '¿Equipo de sonido?'),
(2, '¿Televisor a color?'),
(3, '¿DVD?'),
(4, '¿Licuadora?'),
(5, '¿Refrigeradora o congeladora?'),
(6, '¿Cocina a gas?'),
(7, '¿Telefono fijo?'),
(8, '¿Plancha Electrica?'),
(9, '¿Lavadora?'),
(10, '¿Computadora?'),
(11, '¿Horno microondas?'),
(12, '¿Internet?'),
(13, '¿Cable?'),
(14, '¿Celular?'),
(15, 'No tiene ninguno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracsocial`
--

CREATE TABLE IF NOT EXISTS `caracsocial` (
  `id_carsoc` int(11) NOT NULL AUTO_INCREMENT,
  `id_pgh` int(11) NOT NULL,
  `apelpat_pob` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `apelmat_pob` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nomb_pob` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `fechnac_pob` date NOT NULL,
  `añocumplid_pob` smallint(6) NOT NULL,
  `mescumplid_pob` smallint(6) NOT NULL,
  `desc_td` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `numnucleo_pob` smallint(6) NOT NULL,
  `sexo_pob` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `gestante_pob` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `desc_ec` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `desc_ts` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `desc_idm` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `leerescrib_pob` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `desc_nive` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `ultimoaño_pob` smallint(6) NOT NULL,
  `desc_ocu` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `desc_sec` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `desc_disc` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `desc_prgs` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_carsoc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `combustible`
--

CREATE TABLE IF NOT EXISTS `combustible` (
  `id_comb` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_comb` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_comb`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `combustible`
--

INSERT INTO `combustible` (`id_comb`, `desc_comb`) VALUES
(1, '¿Electricidad?'),
(2, '¿Gas?'),
(3, '¿Kerosene?'),
(4, '¿Carbon?'),
(5, '¿Leña?'),
(6, '¿Bosta o estiercol?'),
(7, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `discapacidad`
--

CREATE TABLE IF NOT EXISTS `discapacidad` (
  `id_disc` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_disc` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_disc`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `discapacidad`
--

INSERT INTO `discapacidad` (`id_disc`, `desc_disc`) VALUES
(1, 'Visual parcial o total'),
(2, 'Para oir total o parcial'),
(3, 'Para hablar total o parcial'),
(4, 'Para usar brazos y manos o piernas y pies'),
(5, 'Mental o intelectual'),
(6, 'No tiene discapacidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadocivil`
--

CREATE TABLE IF NOT EXISTS `estadocivil` (
  `id_ec` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_ec` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_ec`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `estadocivil`
--

INSERT INTO `estadocivil` (`id_ec`, `desc_ec`) VALUES
(1, 'Soltero'),
(2, 'Casado(a)'),
(3, 'Conviviente'),
(4, 'Separado(a)'),
(5, 'Divorsiado(a)'),
(6, 'Viudo(a)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idioma`
--

CREATE TABLE IF NOT EXISTS `idioma` (
  `id_idm` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_idm` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_idm`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `idioma`
--

INSERT INTO `idioma` (`id_idm`, `desc_idm`) VALUES
(1, 'Quechua'),
(2, 'Aymara'),
(3, 'Ashaninka'),
(4, 'Castellano'),
(5, 'Idioma extranjero'),
(6, 'Es sordomudo(a)'),
(7, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materialpiso`
--

CREATE TABLE IF NOT EXISTS `materialpiso` (
  `id_matps` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_matps` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_matps`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `materialpiso`
--

INSERT INTO `materialpiso` (`id_matps`, `desc_matps`) VALUES
(1, '¿Parquet o madera pulida?'),
(2, '¿Laminas asfalticas, vinilicos o similares?'),
(3, '¿Losetas, terrazos o similares?'),
(4, '¿Madera?'),
(5, '¿Cemento?'),
(6, '¿Tierra?'),
(7, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materialtecho`
--

CREATE TABLE IF NOT EXISTS `materialtecho` (
  `id_mattch` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_mattch` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_mattch`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `materialtecho`
--

INSERT INTO `materialtecho` (`id_mattch`, `desc_mattch`) VALUES
(1, '¿Concreto Armado?'),
(2, '¿Madera?'),
(3, '¿Tejas?'),
(4, '¿Plancha de calamina, fibra de cemento o similares?'),
(5, '¿Caña o estera con torta de barro?'),
(6, '¿Estera?'),
(7, '¿Paja, hojas de palmera?'),
(8, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materialviv`
--

CREATE TABLE IF NOT EXISTS `materialviv` (
  `id_matv` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_matv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_matv`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `materialviv`
--

INSERT INTO `materialviv` (`id_matv`, `desc_matv`) VALUES
(1, '¿Ladrillo o bloque de cemento?'),
(2, '¿Piedra o sillar con cal o cemento?'),
(3, '¿Adobe o tapia?'),
(4, '¿Quincha?'),
(5, '¿Piedra con barro?'),
(6, '¿Madera?'),
(7, '¿Estera?'),
(8, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `niveleducativo`
--

CREATE TABLE IF NOT EXISTS `niveleducativo` (
  `id_nive` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_nive` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_nive`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `niveleducativo`
--

INSERT INTO `niveleducativo` (`id_nive`, `desc_nive`) VALUES
(1, 'Ninguno'),
(2, 'Inicial'),
(3, 'Primaria'),
(4, 'Secundaria'),
(5, 'Superior no Universitaria'),
(6, 'Superior Universitaria'),
(7, 'Post Grado u otro similar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ocupacion`
--

CREATE TABLE IF NOT EXISTS `ocupacion` (
  `id_ocu` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_ocu` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_ocu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `ocupacion`
--

INSERT INTO `ocupacion` (`id_ocu`, `desc_ocu`) VALUES
(1, 'Trabajador dependiente'),
(2, 'Trabajador independiente'),
(3, 'Empleador'),
(4, 'Trabajador del hogar'),
(5, 'Trabajador familiar no remunerado'),
(6, 'Desempleado'),
(7, 'Dedicado a los quehaceres del hogar'),
(8, 'Estudiante'),
(9, 'Jubilado'),
(10, 'Sin actividad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `padrongh`
--

CREATE TABLE IF NOT EXISTS `padrongh` (
  `id_pgh` int(11) NOT NULL AUTO_INCREMENT,
  `num_pgh` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `idUbigeo` int(11) NOT NULL,
  `desc_centpob` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `cod_centpob` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `cat_centpob` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `desc_nucleourb` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `cat_nucleourb` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `cong_ubicens` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `zona_ubicens` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `mz_ubicens` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `numfrentemz_ubicens` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `numvivienda_ubicens` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `canthogar_ubicens` smallint(6) NOT NULL,
  `numhogar_ubicens` smallint(6) NOT NULL,
  `apelnombinform_ubicens` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `num_ubicens` smallint(6) NOT NULL,
  `desc_tv` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nombrevia_ubicens` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `numpuerta_ubicens` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `block_ubicens` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `piso_ubicens` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `interior_ubicens` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `manzana_ubicens` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `lote_ubicens` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `km_ubicens` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_ubicens` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `apelnom_empad_persresp` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `dni nom_empad_persresp` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `reside_inform` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `numpisos_inform` smallint(6) NOT NULL,
  `colorfrontis_inform` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `firma_inform` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `nofirma_inform` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `fechvisit1_emp` date NOT NULL,
  `desc_rsv1` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fechvisit2_emp` date NOT NULL,
  `desc_rsv2` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fechvisit3_emp` date NOT NULL,
  `desc_rsv3` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fechresfinal_emp` date NOT NULL,
  `desc_rsvf` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `otroresfinal_emp` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `desc_tipv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otrovivnd_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `desc_matv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otromaterviv_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `desc_mattch` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otromatertech_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `desc_matps` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otromaterpiso_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `desc_alum` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otroalumbr_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `desc_ag` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otroagua_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `desc_sh` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `hrsdemora_viv` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `habitac_hog` smallint(6) NOT NULL,
  `desc_comb` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `otrocombust_hog` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `desc_art` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `suminiserv_hog` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `otrosuminiserv_hog` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `numhomb_hog` smallint(6) NOT NULL,
  `nummuje_hog` smallint(6) NOT NULL,
  `numtotal_hog` smallint(6) NOT NULL,
  PRIMARY KEY (`id_pgh`),
  KEY `idUbigeo` (`idUbigeo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parentesco`
--

CREATE TABLE IF NOT EXISTS `parentesco` (
  `id_paren` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_paren` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_paren`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `parentesco`
--

INSERT INTO `parentesco` (`id_paren`, `desc_paren`) VALUES
(1, 'Jefe'),
(2, 'Conyuge'),
(3, 'Hijo(a) '),
(4, 'Yerno o nuera'),
(5, 'Nieto(a)'),
(6, 'Padres o suegros'),
(7, 'Hermano(a)'),
(8, 'Trabajador del hogar'),
(9, 'Pensionista'),
(10, 'Otros parientes'),
(11, 'Otros no parientes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programasocial`
--

CREATE TABLE IF NOT EXISTS `programasocial` (
  `id_prgs` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_prgs` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_prgs`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `programasocial`
--

INSERT INTO `programasocial` (`id_prgs`, `desc_prgs`) VALUES
(1, 'Vaso de leche'),
(2, 'Comedor popular'),
(3, 'Desayuno o almuerzo escolar'),
(4, 'Papilla o Yapita'),
(5, 'Canasta alimentaria'),
(6, 'Juntos'),
(7, 'Techo propio o mi vivienda'),
(8, 'Pension 65'),
(9, 'Cuna mas'),
(10, 'Otros'),
(11, 'Ninguno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resvisita`
--

CREATE TABLE IF NOT EXISTS `resvisita` (
  `id_rsv` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_rsv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_rsv`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `resvisita`
--

INSERT INTO `resvisita` (`id_rsv`, `desc_rsv`) VALUES
(1, 'Completa'),
(2, 'Incompleta'),
(3, 'Rechazo'),
(4, 'Ausente'),
(5, 'No se inicio la entrevista'),
(6, 'Vivienda desocupada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sector`
--

CREATE TABLE IF NOT EXISTS `sector` (
  `id_sec` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_sec` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_sec`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `sector`
--

INSERT INTO `sector` (`id_sec`, `desc_sec`) VALUES
(1, 'Agricola'),
(2, 'Pecuario'),
(3, 'Forestal'),
(4, 'Pesqueria'),
(5, 'Mineria'),
(6, 'Artesanal'),
(7, 'Comercial'),
(8, 'Servicios'),
(9, 'Otros'),
(10, 'Estado(Gob.)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sshhviv`
--

CREATE TABLE IF NOT EXISTS `sshhviv` (
  `id_sh` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_sh` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_sh`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `sshhviv`
--

INSERT INTO `sshhviv` (`id_sh`, `desc_sh`) VALUES
(1, '¿Red publica dentro de la vivienda?'),
(2, '¿Red publica fuera de la vivienda, pero dentro del edificio?'),
(3, '¿Pozo septico?'),
(4, '¿Pozo ciego o negro o letrina?'),
(5, '¿Rio, acequia o canal?'),
(6, 'No tiene');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodoc`
--

CREATE TABLE IF NOT EXISTS `tipodoc` (
  `id_td` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_td` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_td`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tipodoc`
--

INSERT INTO `tipodoc` (`id_td`, `desc_td`) VALUES
(1, 'DNI'),
(2, 'Partida de Nacimiento'),
(3, 'Carnet de extranjería'),
(4, 'No tiene');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposeguro`
--

CREATE TABLE IF NOT EXISTS `tiposeguro` (
  `id_ts` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_ts` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_ts`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `tiposeguro`
--

INSERT INTO `tiposeguro` (`id_ts`, `desc_ts`) VALUES
(1, 'EsSalud'),
(2, 'FF.AA.-PNP'),
(3, 'Seguro Privado'),
(4, 'Seguro Integral de Salud (SIS)'),
(5, 'Otro'),
(6, 'No tiene');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipovia`
--

CREATE TABLE IF NOT EXISTS `tipovia` (
  `id_tv` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_tv` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_tv`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `tipovia`
--

INSERT INTO `tipovia` (`id_tv`, `desc_tv`) VALUES
(1, 'Avenida'),
(2, 'Jiron'),
(3, 'Calle'),
(4, 'Pasaje'),
(5, 'Carretera'),
(6, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipovivienda`
--

CREATE TABLE IF NOT EXISTS `tipovivienda` (
  `id_tipv` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc-tipv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_tipv`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `tipovivienda`
--

INSERT INTO `tipovivienda` (`id_tipv`, `desc-tipv`) VALUES
(1, 'Casa independiente'),
(2, 'Departamento o edificio'),
(3, 'Vivienda en quinta'),
(4, 'Vivienda en casa de vecindad'),
(5, 'Choza o cabaña'),
(6, 'Vivienda improvisada'),
(7, 'Local no destinado para habitación humana'),
(8, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubigeo`
--

CREATE TABLE IF NOT EXISTS `ubigeo` (
  `idubigeo` int(11) NOT NULL AUTO_INCREMENT,
  `id1` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `id2` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `id3` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `depto` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `prov` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `distri` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idubigeo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viviendaes`
--

CREATE TABLE IF NOT EXISTS `viviendaes` (
  `id_ves` smallint(6) NOT NULL AUTO_INCREMENT,
  `desc_ves` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_ves`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `viviendaes`
--

INSERT INTO `viviendaes` (`id_ves`, `desc_ves`) VALUES
(1, '¿Alquilada?'),
(2, '¿Propia pagandola a plazos?'),
(3, '¿Propia totalmente pagada?'),
(4, '¿Propia por invasion?'),
(5, '¿Cedida por el centro de trabajo?'),
(6, '¿Cedida por otro hogar o institucion?'),
(7, 'Otro');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `padrongh`
--
ALTER TABLE `padrongh`
  ADD CONSTRAINT `padrongh_ibfk_3` FOREIGN KEY (`idUbigeo`) REFERENCES `ubigeo` (`idubigeo`),
  ADD CONSTRAINT `padrongh_ibfk_1` FOREIGN KEY (`idUbigeo`) REFERENCES `ubigeo` (`idubigeo`),
  ADD CONSTRAINT `padrongh_ibfk_2` FOREIGN KEY (`idUbigeo`) REFERENCES `ubigeo` (`idubigeo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
