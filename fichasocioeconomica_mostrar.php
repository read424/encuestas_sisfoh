<?php
require_once('menu.php');
?>
<div class="wrapper">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="page-title-box">
               <div class="btn-group pull-right">
                  <ol class="breadcrumb hide-phone p-0 m-0">
                     <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                     <li class="breadcrumb-item active">Registro</li>
                     <li class="breadcrumb-item active">
                     		<a  name="btneliminar" class="btn btn-primary btn-sm get-code" href="fichasocioeconomica_insertar.php" style="color: white;"><i class="fa fa-plus-circle" title="Nuevo Registro" data-toggle="tooltip"> Nuevo Registro</i></a>
                 		</li>
                  </ol>
               </div>
               <h4 class="page-title">Listado de Registros de Ficha SocioEconómica</h4>
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-lg-12">
            <div class="card m-b-20">
               <div class="card-block">
                  <!-- <h4 class="mt-0 header-title">Default Tabs</h4>-->
                  <ul class="nav nav-tabs" role="tablist">
                     <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Lista de Registros</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active p-3" id="home" role="tabpanel">
                      <table id="table_encuestas" class="table table-striped table-bordered display" width="100%">
                      </table>
                      <form name="form_editar" id="form_editar" action="./fichasocioeconomica_insertar.php?oper=editar" method="POST">
                        <input type="hidden" name="id_pgh" id="id_pgh" value="" />
                        <input type="submit" id="btn_editar" name="btn_editar" value="enviar" style="display:none;" />
                      </form>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
require_once('pie2.php');
?>
<script type="text/javascript">
  $(document).ready(function(){
    
    var table=$('#table_encuestas').DataTable({
        responsive:true,
        paging: true,
        searching: false,
        ajax:{
          url:'./api/padronhogar.php?oper=grid',
          type:'POST',
          dataType:'json'
        },
        columns:[
          {title:'IDPGH', width:'1%', data:'id_pgh', visible:false},
          {title:'ID', width:'1%', data:'id_carsoc', visible:false},
          {title:'Tip. Doc.', width:'8%', data:'tip_doc'},
          {title:'Nº Doc.', width:'8%', data:'num_doc'},
          {title: 'Apellidos y Nombres', width:'20%', data:'apenom'},
          {title:'Distrito', width:'20%', data:'ubica', visible:false},
          {title:'Informante', width:'22%', data:'apenominf'},
          {title:'Fec. Vis.', width:'5%', data:'fec_vis'},
          {title:'Result. Vis.', width:'9%', data:'result_vis'},
          {width:'4%',defaultContent: "<div class='btn-group' role='group'><button type='button' class='btn btn-sm btn-primary btn-editar'><i class='fa fa-pencil'></i></button><button type='button' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></button></div>"}
        ]      
    });
    $("#table_encuestas tbody").on('click', '.btn-editar', function(){
      var row=table.row($(this).parents('tr')).data();
      $("#id_pgh", $("#form_editar")).val(row.id_pgh);
      $("#btn_editar", $("#form_editar")).trigger('click');
    });
   
  });
</script>