-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-09-2018 a las 00:38:52
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sysencuesta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracsocial`
--

DROP TABLE IF EXISTS `caracsocial`;
CREATE TABLE `caracsocial` (
  `id_carsoc` int(11) NOT NULL,
  `id_pgh` int(11) NOT NULL,
  `apelpat_pob` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `apelmat_pob` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nomb_pob` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `fechnac_pob` date NOT NULL,
  `aniocumplid_pob` smallint(6) NOT NULL,
  `mescumplid_pob` smallint(6) NOT NULL,
  `desc_td` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `numdoc_pob` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `desc_paren` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `numnucleo_pob` smallint(6) NOT NULL,
  `sexo_pob` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `gestante_pob` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `desc_ec` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `desc_ts` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `desc_idm` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `leerescrib_pob` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `desc_nive` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `ultimoanio_pob` smallint(6) NOT NULL,
  `desc_ocu` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `desc_sec` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `desc_disc` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `desc_prgs` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `padrongh`
--

DROP TABLE IF EXISTS `padrongh`;
CREATE TABLE `padrongh` (
  `id_pgh` int(11) NOT NULL,
  `num_pgh` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `idUbigeo` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `desc_centpob` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `cod_centpob` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `cat_centpob` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `desc_nucleourb` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `cat_nucleourb` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `cong_ubicens` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `zona_ubicens` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `mz_ubicens` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `numfrentemz_ubicens` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `numvivienda_ubicens` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `canthogar_ubicens` smallint(6) NOT NULL,
  `numhogar_ubicens` smallint(6) NOT NULL,
  `apelnombinform_ubicens` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `num_ubicens` smallint(6) NOT NULL,
  `desc_tv` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `nombrevia_ubicens` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `numpuerta_ubicens` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `block_ubicens` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `piso_ubicens` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `interior_ubicens` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `manzana_ubicens` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `lote_ubicens` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `km_ubicens` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_ubicens` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `apelnom_empad_persresp` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `dni_nom_empad_persresp` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `reside_inform` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `numpisos_inform` smallint(6) NOT NULL,
  `colorfrontis_inform` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `firma_inform` enum('SI','NO') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'SI',
  `nofirma_inform` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `fechvisit1_emp` date NOT NULL,
  `desc_rsv1` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fechvisit2_emp` date NOT NULL,
  `desc_rsv2` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fechvisit3_emp` date NOT NULL,
  `desc_rsv3` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fechresfinal_emp` date NOT NULL,
  `desc_rsvf` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `otroresfinal_emp` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `id_tipovivienda` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otrovivnd_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `id_ves` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `espec_vivienda_es` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_matv` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `otromaterviv_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `id_mattch` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `otromatertech_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `id_matps` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otromaterpiso_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `id_alum` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otroalumbr_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `id_ag` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otroagua_viv` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `id_sh` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `hrsdemora_viv` enum('0','1') COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `habitac_hog` smallint(6) NOT NULL,
  `id_comb` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `otrocombust_hog` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `desc_art` text COLLATE utf8_spanish_ci NOT NULL,
  `tipo_servsuministro` enum('0','1','2') COLLATE utf8_spanish_ci NOT NULL DEFAULT '0' COMMENT '0=> no tiene, 1=> agua, 2=> luz',
  `num_servsuministro` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `numhomb_hog` smallint(6) NOT NULL,
  `nummuje_hog` smallint(6) NOT NULL,
  `numtotal_hog` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `caracsocial`
--
ALTER TABLE `caracsocial`
  ADD PRIMARY KEY (`id_carsoc`);

--
-- Indices de la tabla `padrongh`
--
ALTER TABLE `padrongh`
  ADD PRIMARY KEY (`id_pgh`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `caracsocial`
--
ALTER TABLE `caracsocial`
  MODIFY `id_carsoc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `padrongh`
--
ALTER TABLE `padrongh`
  MODIFY `id_pgh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
