<?php
	require_once('menu.php');

   	$consulta = "SELECT * FROM usuario where activo='1'";
    $resultado = $conexion -> query($consulta);

    $consulta2 = "SELECT * FROM usuario where activo='0'";
    $resultado2 = $conexion -> query($consulta2);
?>
<div class="wrapper">
 <div class="container">
    <div class="row">
       <div class="col-sm-12">
          <div class="page-title-box">
             <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                   <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                   <li class="breadcrumb-item active">Usuarios</li>
                   <li class="breadcrumb-item active">
                   		<a  name="btneliminar" class="btn btn-primary btn-sm get-code" href="usuario_mostrar.php" style="color: white;"><i class="fa fa-plus-circle" title="Listar Usuarios" data-toggle="tooltip"> Listar Usuarios</i></a>
               		</li>
                </ol>
             </div>
             <h4 class="page-title">Registrar Nuevo Usuario</h4>
          </div>
       </div>
    </div>

    <div class="row">
    	<!-- Primera Columna -->
           <div class="col-8">
              <div class="card m-b-20">
                 <div class="card-block">
                    <h4 class="mt-0 header-title">datos Personales</h4>
                    <form class="" action="#" method="POST">
                        <div class="form-group row">
                           <label for="example-text-input" class="col-sm-3 col-form-label" >Nick</label>
                           <div class="col-sm-9"> 
                           		<input type="text" name="alias_usuar" class="form-control" required placeholder="Nick del Usuario" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="example-search-input" class="col-sm-3 col-form-label">Password</label>
                           <div class="col-sm-9">  
                           	<input type="password" name="clave_usuar" id="pass2" class="form-control" required placeholder="Contraseña" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="example-search-input" class="col-sm-3 col-form-label">Repita Password</label>
                           <div class="col-sm-9">  
                           	<input type="password" name="clave_usuar2" class="form-control" required data-parsley-equalto="#pass2" placeholder="Repita Contraseña" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="example-text-input" class="col-sm-3 col-form-label" >Nombres de Usuario</label>
                           <div class="col-sm-9"> 
                           		<input type="text" name="nombre_usuar" class="form-control" required placeholder="Nombre del Usuario" />
                           </div>
                        </div> 
                        <div class="form-group row">
                           <label for="example-text-input" class="col-sm-3 col-form-label" >Apellido de Usuario</label>
                           <div class="col-sm-9"> 
                           		<input type="text" name="apellido_usuar" class="form-control" required placeholder="Apellidos del Usuario" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="example-text-input" class="col-sm-3 col-form-label" >DNI de Usuario</label>
                           <div class="col-sm-9"> 
                           		<input type="text" name="dni_usuar" class="form-control " required placeholder="DNI del Usuario" data-parsley-maxlength="8" data-parsley-type="digits" maxlength="8" minlength="8" style="width: 150px;" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="example-text-input" class="col-sm-3 col-form-label" >Dirección de Usuario</label>
                           <div class="col-sm-9"> 
                           		<input type="text" name="direccion_usuar" class="form-control"  placeholder="Dirección del Usuario" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="example-text-input" class="col-sm-3 col-form-label" >Teléfono / Celular</label>
                           <div class="col-sm-9"> 
                           		<input type="tel" name="telefono_usuar" id="example-tel-input" class="form-control"  placeholder="Teléfono / Celular del Usuario" data-parsley-type="number" />
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="example-text-input" class="col-sm-3 col-form-label" >E-Mail de Usuario</label>
                           <div class="col-sm-9"> 
                           		<input type="email" name="email_usuar" class="form-control"  placeholder="Correo Electrónico del Usuario" />
                           </div>
                        </div>		                        
                        <div class="form-group row">
                           <label class="col-sm-3 col-form-label">Rol de Usuario</label>
                           <div class="col-sm-9">
                              <select class="custom-select " name="rol_usuar">
                                 <option>Administrador</option>
                                 <option>Secretaria</option>
                                 <option>Otros</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group m-b-0" align="center">
                          <div> 
                          	<button type="submit" class="btn btn-primary waves-effect waves-light" name="btnregistrar">  <i class="fa fa fa-user-o"></i> Registrar Usuario </button>
                          </div>
                       </div>
                     </form>
                 </div>
              </div>
           </div>
       <!-- FIN Primera Columna -->

       <!-- Segunda Columna -->
       <div class="col-4">
          <div class="card m-b-20">
             <div class="card-block">
                <h4 class="mt-0 header-title">...</h4>
                
             </div>
          </div>
       </div>
		<!-- FIN Segunda Columna -->

    </div>
 </div>
</div>

<?php
   require_once('pie2.php');
?>