<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <title>Sistema de Gestión</title>
      <meta content="Admin Dashboard" name="description" />
      <meta content="Themesbrand" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="assets/images/favicon.ico">
      <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="assets/css/style.css" rel="stylesheet" type="text/css">
   </head>
   <body class="fixed-left">
      <div id="preloader">
         <div id="status">
            <div class="spinner"></div>
         </div>
      </div>
      <div class="accountbg"></div>
      <div class="wrapper-page">
         <div class="card">
            <div class="card-block">
               <h3 class="text-center m-0"> <a href="index.html" class="logo logo-admin"><img src="images/logo1.png" height="80" alt="logo"></a></h3>
               <div class="p-3">
                  <h4 class="text-muted font-18 m-b-5 text-center">Restablecer la contraseña</h4>
                  <p class="text-muted text-left">Ingrese su correo electrónico y se le enviarán las instrucciones!</p>
                  <form class="form-horizontal m-t-30" action="#">
                     <div class="form-group"> <label for="useremail">Email</label> <input type="email" class="form-control" id="useremail" placeholder="Enter email"></div>
                     <div class="form-group row m-t-20">
                        <div class="col-12 text-right"> <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Restablecer</button></div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="m-t-40 text-center">
           <p class="text-white">© 2018  <i class="mdi mdi-ghost text-danger"></i> &#80;&#115;&#68;&#115; -CETIC</p>
         </div>
      </div>
      <script src="assets/js/jquery.min.js"></script> 
      <script src="assets/js/tether.min.js"></script> 
      <script src="assets/js/bootstrap.min.js"></script> 
      <script src="assets/js/modernizr.min.js"></script> 
      <script src="assets/js/jquery.slimscroll.js"></script> 
      <script src="assets/js/waves.js"></script> 
      <script src="assets/js/jquery.nicescroll.js"></script> 
      <script src="assets/js/jquery.scrollTo.min.js"></script> 
      <script src="assets/js/app.js"></script> 
   </body>
</html>