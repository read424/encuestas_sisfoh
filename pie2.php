  <footer class="footer">
    <div class="container">
        <div class="row">
          <div class="col-12"> 2018 © &#80;&#115;&#68;&#115; -CETIC <span class="text-muted hidden-xs-down pull-right"> Todos los derechos reservados <i class="mdi mdi-ghost text-danger"></i> &#80;&#115;&#68;&#115;</span></div>
        </div>
    </div>
  </footer>
  <script src="assets/js/jquery.min.js"></script> 
  <script src="assets/plugins/moment/moment.js"></script> 
  <script src="assets/js/toastr.min.js"></script> 
  <script src="assets/js/tether.min.js"></script> 
  <script src="assets/js/bootstrap.min.js"></script> 
  <script src="assets/js/modernizr.min.js"></script> 
  <script src="assets/js/waves.js"></script> 
  <script src="assets/js/jquery.slimscroll.js"></script> 
  <script src="assets/js/jquery.nicescroll.js"></script> 
  <script src="assets/js/jquery.scrollTo.min.js"></script> 
  <script src="assets/plugins/parsleyjs/parsley.min.js"></script> 
  <!-- Forms avanzados -->
  <script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
  <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script> 
  <script src="assets/plugins/select2/js/select2.min.js" ></script> 
  <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script> 
  <script src="assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js"></script> 
  <script src="assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script> 
  <script src="assets/pages/form-advanced.js"></script> 
  <!-- FIN Forms avanzados -->
  <!-- DATATABLES -->
  <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script> 
  <script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script> 
  <script src="assets/plugins/datatables/dataTables.buttons.min.js"></script> 
  <script src="assets/plugins/datatables/buttons.bootstrap4.min.js"></script> 
  <script src="assets/plugins/datatables/jszip.min.js"></script> 
  <script src="assets/plugins/datatables/pdfmake.min.js"></script> 
  <script src="assets/plugins/datatables/vfs_fonts.js"></script> 
  <script src="assets/plugins/datatables/buttons.html5.min.js"></script> 
  <script src="assets/plugins/datatables/buttons.print.min.js"></script> 
  <script src="assets/plugins/datatables/buttons.colVis.min.js"></script> 
  <script src="assets/plugins/datatables/dataTables.responsive.min.js"></script> 
  <script src="assets/plugins/datatables/responsive.bootstrap4.min.js"></script> 
  <script src="assets/pages/datatables.init.js"></script>
  <!-- FIN DATATABLES -->       
	<!-- FORMS WIZARD -->  
   <!--script src="assets/plugins/jquery-steps/jquery.steps.min.js"></script> 
   <script>
      $(function() {$("#form-horizontal").steps({headerTag:"h3",bodyTag:"fieldset",transitionEffect:"slide"});});
   </script--> 
<!-- FIN FORMS WIZARD -->

<!-- WIDGETS -->
  <script src="assets/plugins/peity-chart/jquery.peity.min.js"></script> 
  <script type="text/javascript" src="assets/plugins/d3/d3.min.js"></script> 
  <script type="text/javascript" src="assets/plugins/c3/c3.min.js"></script> 
  <script src="assets/plugins/jquery-knob/excanvas.js"></script> 
  <script src="assets/plugins/jquery-knob/jquery.knob.js"></script> 
  <script src="assets/pages/widget-init.js"></script> 
 <!-- FIN WIDGETS -->
  <script src="assets/js/angular.min.js"></script>
  <script src="assets/js/angular-route.min.js"></script>
  <script src="assets/js/app.js"></script> 
</body>

</html>