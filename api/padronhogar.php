<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
$a_operacion=array("guardar", "guardarpoblacion", "grid");
if(in_array($_GET['oper'], $a_operacion)){
    try{
        require './class/padronhogar.class.php';
        require './class/conectorbd.class.php';
        $oPadron= new padronhogar();
        $oConector= new conectorbd();
        $init_stmt=$oConector->stmt_init();
        $response_json=array("success"=>false, "messages"=>"Opps!, estas intentando algo inusual en el sistema", "rows"=>array(), "id"=>-1, "affected_rows"=>-1, "num_rows"=>-1);
        switch($_GET['oper']){
            case 'grid':
                $response_json=array("draw"=>-1, "recordsTotal"=>0, "recordsFiltered"=>0, "data"=>array());
                $sql=$oPadron->listar();
                if(!$init_stmt->prepare($sql))
                    throw new Exception($init_stmt->error);
                $init_stmt->execute();
                $result_stmt=$init_stmt->get_result();
                $response_json['recordsTotal']=$result_stmt->num_rows;
                $response_json['recordsFiltered']=$result_stmt->num_rows;
                if($response_json['recordsTotal']==0)
                    break;
                while($rows=$result_stmt->fetch_assoc()){
                    if(isset($rows['fechvisit2_emp'])){
                        $rows['fec_vis']=$rows['fechvisit2_emp'];
                        $rows['result_vis']=$rows['desc_rsv2'];
                    }
                    unset($rows['fechvisit2_emp']);
                    unset($rows['desc_rsv2']);
                    if(isset($rows['fechvisit3_emp'])){
                        $rows['fec_vis']=$rows['fechvisit3_emp'];
                        $rows['result_vis']=$rows['desc_rsv3'];
                    }                    
                    unset($rows['fechvisit3_emp']);
                    unset($rows['desc_rsv3']);
                    $rows['tip_doc']=utf8_encode($rows['tip_doc']);
                    array_push($response_json['data'], $rows);
                }
            break;
            case 'guardarpoblacion':
                if(!isset($_POST['id_pgh'], $_POST['id_carsoc'], $_POST['apellido_paterno'], $_POST['apellido_materno'], $_POST['nombres'], $_POST['fec_nacimiento'], $_POST['edad'], $_POST['meses'], $_POST['tip_documento'], $_POST['num_documento'], $_POST['id_parentesco'], $_POST['num_nucleo'], $_POST['sexo'], $_POST['gestante'], $_POST['edo_civil'], $_POST['tipo_essalud'], $_POST['idioma_natal'], $_POST['leer_escribir'], $_POST['nivel_educativo'], $_POST['ult_grado_cursado'], $_POST['ocupacion_antes'], $_POST['sector_ocupacion'], $_POST['discapacidad'], $_POST['programa_social']) || empty($_POST['id_pgh']))
                    break;
                $response_json['rows']=array('id_carsoc'=>0,'tip_doc'=>'', 'num_doc'=>'', 'apenom'=>'', 'fec_nac'=>'');
                $oConector->setAutocommit(FALSE);
                $sql=$oPadron->agregarpoblacion();
                $a_format_date=array("d/m/Y", "d-m-Y", "Y-m-d", "d-m-Y", "Y/m/d");
                foreach($a_format_date as $format_date){
                    $fec_nac=DateTime::createFromFormat($format_date, $_POST['fec_nacimiento']);
                    if($fec_nac instanceof DateTime){
                        $date_nacimiento=$fec_nac;
                        break;
                    }
                }
                $_POST['fec_nacimiento']=isset($fec_nac)?$fec_nac->format('Y-m-d'):NULL;
                if(!$init_stmt->prepare($sql))
                    throw new Exception($init_stmt->error);
                if(!$init_stmt->bind_param('sssssisssisssssssissssii', $_POST['apellido_paterno'], $_POST['apellido_materno'], $_POST['nombres'], $_POST['fec_nacimiento'], $_POST['edad'], $_POST['meses'], $_POST['tip_documento'], $_POST['num_documento'], $_POST['id_parentesco'], $_POST['num_nucleo'], $_POST['sexo'], $_POST['gestante'], $_POST['edo_civil'], $_POST['tipo_essalud'], $_POST['idioma_natal'], $_POST['leer_escribir'], $_POST['nivel_educativo'], $_POST['ult_grado_cursado'], $_POST['ocupacion_antes'], $_POST['sector_ocupacion'], $_POST['discapacidad'], $_POST['programa_social'], $_POST['id_carsoc'], $_POST['id_pgh']))
                    throw new Exception("Opss!, ocurrio un problema en el sistema", $init_stmt->errno, $init_stmt->error);
                $init_stmt->execute();
                $response_json['success']=true;
                $response_json['affected_rows']=$init_stmt->affected_rows;
                if($init_stmt->affected_rows!=1){
                    $response_json['errno']=$init_stmt->errno;
                    $response_json['messages']=(!empty($response_json['errno']))?$init_stmt->error:"Opss!, ocurrio un problema al registrar la encuesta";
                    break;
                }
                $response_json['messages']="Se guardaron los datos del miembro familiar satisfactoriamente";
                $response_json['id']=$init_stmt->insert_id;
                $a_tip_doc=array(1=>'DNI', 2=>'Part.Nac.-CUI', 3=>'Carné Ex.', 4=>'No tiene doc.');
                $response_json['rows']['id_carsoc']=$init_stmt->insert_id;
                $response_json['rows']['tip_doc']=$a_tip_doc[$_POST['tip_documento']];
                $response_json['rows']['num_doc']=$_POST['num_documento'];
                $response_json['rows']['apenom']=sprintf("%s %s, %s", $_POST['apellido_paterno'], $_POST['apellido_materno'], $_POST['nombres']);
                $response_json['rows']['fec_nac']=$_POST['fec_nacimiento'];
                $oConector->commit();
            break;
            case 'guardar':
                if(!isset($_POST['num_pgh'], $_POST['Id_ubi'], $_POST['desc_centpob'], $_POST['cod_centpob'], $_POST['cat_centpob'], $_POST['desc_nucleourb'], $_POST['cat_nucleourb'], $_POST['cong_ubicens'], $_POST['zona_ubicens'], $_POST['mz_ubicens'], $_POST['numfrentemz_ubicens'], $_POST['numvivienda_ubicens'], $_POST['canthogar_ubicens'], $_POST['numhogar_ubicens'], $_POST['apelnombinform_ubicens'], $_POST['num_ubicens'], $_POST['desc_tv'], $_POST['nombrevia_ubicens'], $_POST['numpuerta_ubicens'], $_POST['block_ubicens'], $_POST['piso_ubicens'], $_POST['interior_ubicens'], $_POST['manzana_ubicens'], $_POST['lote_ubicens'], $_POST['km_ubicens'], $_POST['telefono_ubicens'], $_POST['apelnom_empad_persresp'], $_POST['dninom_empad_persresp'], $_POST['reside_inform'], $_POST['numpisos_inform'], $_POST['colorfrontis_inform'], $_POST['firma_inform'], $_POST['desc_rsv1'], $_POST['nofirma_inform'], $_POST['fechvisit1_emp'], $_POST['fechvisit2_emp'], $_POST['fechvisit3_emp'], $_POST['desc_rsv3'], $_POST['fechresfinal_emp'], $_POST['desc_rsvf'], $_POST['desc_rsv2'], $_POST['otroresfinal_emp'], $_POST['id_tipovivienda'], $_POST['otro_mat_vivienda'], $_POST['id_mattch'], $_POST['id_ves'], $_POST['espec_vivienda_es'], $_POST['id_matv'], $_POST['otromatertech_viv'], $_POST['otromaterpiso_viv'], $_POST['id_alum'], $_POST['otroalumbr_viv'], $_POST['id_ag'], $_POST['otroagua_viv'], $_POST['id_sh'], $_POST['hrsdemora_viv'], $_POST['habitac_hog'], $_POST['id_comb'], $_POST['otrocombust_hog'], $_POST['desc_art'], $_POST['tipo_servsuministro'], $_POST['num_servsuministro'], $_POST['numhomb_hog'], $_POST['nummuje_hog'], $_POST['numtotal_hog'], $_POST['id_pgh']))
                    break;
                $oConector->setAutocommit(FALSE);
                $sql=$oPadron->agregar();
                $articulos_hogar='';
                if(is_array($_POST['desc_art']) && count($_POST['desc_art'])>0)
                    $articulos_hogar=implode(',', $_POST['desc_art']);
                if(!$init_stmt->prepare($sql))
                    throw new Exception($init_stmt->error);
                if(!$init_stmt->bind_param('ssssssssssssiisisssssssssssssissssssssssssssssssssssssssssisssssiiii', $_POST['num_pgh'], $_POST['Id_ubi'], $_POST['desc_centpob'], $_POST['cod_centpob'], $_POST['cat_centpob'], $_POST['desc_nucleourb'], $_POST['cat_nucleourb'], $_POST['cong_ubicens'], $_POST['zona_ubicens'], $_POST['mz_ubicens'], $_POST['numfrentemz_ubicens'], $_POST['numvivienda_ubicens'], $_POST['canthogar_ubicens'], $_POST['numhogar_ubicens'], $_POST['apelnombinform_ubicens'], $_POST['num_ubicens'], $_POST['desc_tv'], $_POST['nombrevia_ubicens'], $_POST['numpuerta_ubicens'], $_POST['block_ubicens'], $_POST['piso_ubicens'], $_POST['interior_ubicens'], $_POST['manzana_ubicens'], $_POST['lote_ubicens'], $_POST['km_ubicens'], $_POST['telefono_ubicens'], $_POST['apelnom_empad_persresp'], $_POST['dninom_empad_persresp'], $_POST['reside_inform'], $_POST['numpisos_inform'], $_POST['colorfrontis_inform'], $_POST['firma_inform'], $_POST['nofirma_inform'], $_POST['fechvisit1_emp'], $_POST['desc_rsv1'], $_POST['fechvisit2_emp'], $_POST['desc_rsv2'], $_POST['fechvisit3_emp'], $_POST['desc_rsv3'], $_POST['fechresfinal_emp'], $_POST['desc_rsvf'], $_POST['otroresfinal_emp'], $_POST['id_tipovivienda'], $_POST['otrovivnd_viv'], $_POST['id_ves'], $_POST['espec_vivienda_es'], $_POST['id_matv'], $_POST['otro_mat_vivienda'], $_POST['id_mattch'],  $_POST['otromatertech_viv'], $_POST['id_matps'], $_POST['otromaterpiso_viv'], $_POST['id_alum'], $_POST['otroalumbr_viv'], $_POST['id_ag'], $_POST['otroagua_viv'], $_POST['id_sh'], $_POST['hrsdemora_viv'], $_POST['habitac_hog'], $_POST['id_comb'], $_POST['otrocombust_hog'], $articulos_hogar, $_POST['tipo_servsuministro'], $_POST['num_servsuministro'], $_POST['numhomb_hog'], $_POST['nummuje_hog'], $_POST['numtotal_hog'], $_POST['id_pgh']))
                    throw new Exception("Opss!, ocurrio un problema en el sistema", $init_stmt->errno, $init_stmt->error);
                $init_stmt->execute();
                $response_json['success']=true;
                $response_json['affected_rows']=$init_stmt->affected_rows;
                if($init_stmt->affected_rows!=1){
                    $response_json['errno']=$init_stmt->errno;
                    if(!empty($response_json['errno']))
                        $response_json['messages']=$init_stmt->error;
                    else
                        $response_json['messages']="Opss!, ocurrio un problema al registrar la encuesta";
                    break;
                }
                $response_json['messages']="Los datos fueron guardado satisfactoriamente";
                $response_json['id']=$init_stmt->insert_id;
                $oConector->commit();
            break;
        }
        echo json_encode($response_json);
    }catch(Exception $e){
        echo $e->getMessage();
    }
}
?>