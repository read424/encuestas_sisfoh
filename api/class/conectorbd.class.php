<?php
require './configuracion.php';
class conectorbd extends MySQLi {
    private $status_commit;

    public function __construct(){
        parent::__construct(HOSTDB, USERDB, PASSDB, NAMEDB);
        $this->set_charset('utf-8');

    }

    public function setAutocommit($status_commit=FALSE){
		$this->status_commit=$status_commit;
		$this->autocommit($this->status_commit);
    }
    
    
    public function __destroy(){
		if($this->status_commit==FALSE)
			$this->rollback();
		$this->close();
	}    
}
?>