<?php
class padronhogar{

    public function __construct(){

    }

    public function agregar(){
        return "INSERT INTO padrongh ( num_pgh , idUbigeo, desc_centpob, cod_centpob, cat_centpob, desc_nucleourb, cat_nucleourb, cong_ubicens, zona_ubicens, mz_ubicens, numfrentemz_ubicens, numvivienda_ubicens, canthogar_ubicens, numhogar_ubicens, apelnombinform_ubicens, num_ubicens, desc_tv, nombrevia_ubicens, numpuerta_ubicens, block_ubicens, piso_ubicens, interior_ubicens, manzana_ubicens, lote_ubicens, km_ubicens, telefono_ubicens, apelnom_empad_persresp, dni_nom_empad_persresp, reside_inform, numpisos_inform, colorfrontis_inform, firma_inform, nofirma_inform, fechvisit1_emp, desc_rsv1, fechvisit2_emp, desc_rsv2, fechvisit3_emp, desc_rsv3, fechresfinal_emp, desc_rsvf, otroresfinal_emp, id_tipovivienda, otrovivnd_viv, id_ves, espec_vivienda_es, id_matv, otromaterviv_viv, id_mattch, otromatertech_viv, id_matps, otromaterpiso_viv, id_alum, otroalumbr_viv, id_ag, otroagua_viv, id_sh, hrsdemora_viv, habitac_hog, id_comb, otrocombust_hog, desc_art, tipo_servsuministro, num_servsuministro, numhomb_hog, nummuje_hog, numtotal_hog, id_pgh) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }
    
    public function agregarpoblacion(){
        return "INSERT INTO caracsocial (apelpat_pob, apelmat_pob, nomb_pob, fechnac_pob, aniocumplid_pob, mescumplid_pob, desc_td, numdoc_pob, desc_paren, numnucleo_pob, sexo_pob, gestante_pob, desc_ec, desc_ts, desc_idm, leerescrib_pob, desc_nive, ultimoanio_pob, desc_ocu, desc_sec, desc_disc, desc_prgs, id_carsoc, id_pgh) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }

    public function listar(){
        return "SELECT p.id_pgh, cs.id_carsoc, p.idUbigeo AS ubica, cs.desc_td AS tip_doc, cs.numdoc_pob AS num_doc, CONCAT_WS(', ', TRIM(CONCAT_WS(' ',cs.apelpat_pob, cs.apelmat_pob)), cs.nomb_pob) AS apenom, p.apelnombinform_ubicens AS apenominf, DATE_FORMAT(p.fechvisit1_emp, '%d-%m-%Y') AS fec_vis, p.desc_rsv1 AS result_vis, DATE_FORMAT(p.fechvisit2_emp, '%d-%m-%Y') AS fechvisit2_emp, p.desc_rsv2, DATE_FORMAT(p.fechvisit3_emp, '%d-%m-%Y') AS fechvisit3_emp, p.desc_rsv3 FROM padrongh AS p INNER JOIN caracsocial AS cs ON cs.id_pgh=p.id_pgh AND cs.desc_paren='Jefe'";
    }
}
?>