<?php
session_start();
require_once('menu.php');
/*if(isset($_SESSION['email'])){
  require("conexion/dbsistema.php");
  if ($_SESSION["s_tipo"]=="Administrador")
  {  
    include('menu.php');
  }
  if ($_SESSION["s_tipo"]=="Vendedor")
  {  
    include('menu_v.php'); 
  }
  if ($_SESSION["s_tipo"]=="Cajero")
  {  
    include('menu_c.php'); 
  }*/


if ( isset($_POST['btnregistrar']) ) {
	function inputSeguro($conexion,$post)
	{
	    $input = htmlentities($post);
	    $seguro = mysqli_real_escape_string ($conexion,$input);
	    return $seguro;
	}
	//$nombre_usuar=$_REQUEST['nombre_usuar'];
	$alias_usuar =  inputSeguro($conexion,$_REQUEST['nick']);
	$clave_usuar1 =  inputSeguro($conexion,$_REQUEST['password']);
	$clave_usuar = password_hash($clave_usuar1, PASSWORD_DEFAULT); 
	$nombre_usuar =  inputSeguro($conexion,$_REQUEST['nombre_usuar']);
	$apellido_usuar =  inputSeguro($conexion,$_REQUEST['apellido_usuar']);
	$dni_usuar =  inputSeguro($conexion,$_REQUEST['dni_usuar']);
	$direccion_usuar =  inputSeguro($conexion,$_REQUEST['direccion_usuar']);
	$telefono_usuar =  inputSeguro($conexion,$_REQUEST['telefono_usuar']);
	$email_usuar =  inputSeguro($conexion,$_REQUEST['email_usuar']);
	$rol_usuar=$_REQUEST['rol_usuario'];

  	$consultaCat = "SELECT * FROM usuario WHERE alias_usuar='$nomb_usr'";
      $resultadoCat = $conexion -> query($consultaCat);
      $fila = $resultadoCat -> fetch_array();

    $row_cnt = $resultadoCat->num_rows;
    if ($row_cnt>0  )
    {
        $msgSQL="<br>Ya existe un Usuario con este mismo NICK en la BD. <br>Intente de Nuevo Por Favor<br>";
        $alert="callout callout-danger";
        $link= "<META HTTP-EQUIV=Refresh CONTENT=2;URL=usuario_insertar.php>";
    }
    else
    {
      if (empty(trim($nom_usr)))
      {
        $msgSQL= "No escribió un NICK para el Usuario";
        $alert="callout callout-danger";
        $link= "<META HTTP-EQUIV=Refresh CONTENT=2;URL=usuario_insertar.php>";
      }
      else
      {
        $insert="INSERT INTO usuarios(nom_usr,pws_usr, nomb_usr, apel_usr, dni_usr, tlfcel_usr, direcc_usr, lugar_usr, idubigeo, id_niv, id_per,  fechreg_usr, Id_dep) VALUES('$nom_usr','$clave_usuar', '$nomb_usr','$apel_usr', '$dni_usr','$tlfcel_usr', '$direcc_usr','$lugar_usr', '$idubigeo', '$id_niv', '$id_per',  '$fechreg_usr','$Id_dep')";
        if ($conexion->query($insert) === TRUE) {
            $msgSQL="Enhorabuena, la acción ha sido llevada a cabo con éxito";
            $alert="alert alert-success alert-dismissible fade show";
            $link= "<META HTTP-EQUIV=Refresh CONTENT=2;URL=usuario_mostrar.php>";
          } 
        else {
              echo "Error: " . $insert . "<br>" . $con->error;
              $msgSQL= "Ha ocurrido un error";
              $alert="alert alert-danger alert-colored";
              $link= "<META HTTP-EQUIV=Refresh CONTENT=2;URL=usuario_insertar.php>";
          }
      }    
        
    }
  }

?>

<div class="wrapper">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="page-title-box">
               <div class="btn-group pull-right">
                  <ol class="breadcrumb hide-phone p-0 m-0">
                     <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                     <li class="breadcrumb-item active">Usuarios</li>
                     <li class="breadcrumb-item active">
                        <a  name="btneliminar" class="btn btn-primary btn-sm get-code" href="usuario_mostrar.php" style="color: white;"><i class="fa fa-plus-circle" title="Listar Usuarios" data-toggle="tooltip"> Listar Usuarios</i></a>
                    </li>
                  </ol>
               </div>
               <h4 class="page-title">Registrar Nuevo Usuario</h4>
            </div>
         </div>
      </div>

      <div class="row">
        <!-- Primera Columna -->
           <div class="col-12">
              <div class="card m-b-20">
                 <div class="card-block">
                    <h4 class="mt-0 header-title">Registrar Datos</h4>
                    <div class="<?php echo $alert; ?>" role="alert">
                      <h4><?php //echo $msgSQL;  ?></h4>

                      <p><?php //echo $link;  ?></p>
                    </div>                    
                 </div>
              </div>
           </div>
         <!-- FIN Primera Columna -->
      </div>
   </div>
</div>
<?php
/*
} 

else {
      //header("Location: usuario_insertar.php"); 
      
      echo "<META HTTP-EQUIV=Refresh CONTENT=2;URL=usuario_insertar.php>";
}
*/
?>
<?php
  require_once('pie.php');
?>
<?php
/*
 }else{
    // Si no está logueado lo redireccion a la página de login.
    header("HTTP/1.1 302 Moved Temporarily");
    echo "<META HTTP-EQUIV=Refresh CONTENT=0;URL=index.php>";
    //header("Location: index.php");
  }
  */
?>
