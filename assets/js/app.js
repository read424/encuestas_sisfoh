/*
 Template Name: Admiry - Bootstrap 4 Admin Dashboard
 Author: Themesbrand
 File: Main js
 */
(function(){
    'use strict';
    angular.module('EncuestasAdmin', [
        'ui.router',
        'EncuestasAdmin.sides',
        'EncuestasAdmin.components'
    ]).config(['$urlRouterProvider', '$httpProvider', '$stateProvider', function($urlRouterProvider, $httpProvider, $stateProvider){
        $stateProvider
        .state('encuestas-grid', {
            url:'/encuestas/grid',
            templateUrl:'./app/encuestas/list/encuestas.html',
            controller:'encuestasListCtrl'
        })
        .state('fichaencuesta', {
            url: '/fichaencuesta',
            templateUrl:'./fichasocioeconomica.html',
            controller:'encuestaCtrl'
        }).state('dashboard', {
            url: '/dashboard',
            templateUrl: 'app/sides/sides.html'
        }).state('dashboard.home', {
            url: '/home',
            templateUrl: 'app/home/home.html'
        })
        .state('login',{
            url:'/login',
            templateUrl:'./app/signin/login.html',
            controller: 'loginCtrl'
        });

        $urlRouterProvider.otherwise('/login');
    }]);
})()

!function(t){
    'use strict';
    function n(){
        t(".navbar-toggle").on("click",function(n){
            t(this).toggleClass("open"), t("#navigation").slideToggle(400)
        }),
        t(".navigation-menu>li").slice(-1).addClass("last-elements"),
        t('.navigation-menu li.has-submenu a[href="#"]').on("click",function(n){
            t(window).width()<992&&(n.preventDefault(),
            t(this).parent("li").toggleClass("open").find(".submenu:first").toggleClass("open"))
        })
    }
    function e(){
        t(window).load(function(){
            t("#status").fadeOut(),
            t("#preloader").delay(350).fadeOut("slow"),
            t("body").delay(350).css({overflow:"visible"})
        })
    }
    function a(){
        t(".slimscroll-noti").slimScroll({height:"230px",position:"right",size:"5px",color:"#98a6ad",wheelStep:10})
    }
    function i(){
        t(".navigation-menu a").each(function(){
            this.href==window.location.href&&(t(this).parent().addClass("active"),
            t(this).parent().parent().parent().addClass("active"),
            t(this).parent().parent().parent().parent().parent().addClass("active"))
        })
    }
    function o(){
        t('[data-toggle="tooltip"]').tooltip(),
        t('[data-toggle="popover"]').popover()
    }
    function l(){
        t(".toggle-search").on("click",function(){
            var n,e=t(this).data("target");
            e&&(n=t(e),n.toggleClass("open"))
        })
    }
    function s(){
        n(),e(),a(),i(),o(),l()
    }
    s()
}(jQuery);
