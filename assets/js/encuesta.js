(function(){
    $(document).ready(function(){
        var tabla_poblacion=$("#table_poblacion").DataTable({
            responsive:true,
            paging: false,
            searching: false,
            data:[],
            columns:[
                {title:'ID', width:'1%', data:'id_carsoc', visible:false},
                {title:'Tip. Doc.', width:'10%', data:'tip_doc'},
                {title:'Nº Doc.', width:'10%', data:'num_doc'},
                {title: 'Apellidos y Nombres', width:'69%', data:'apenom'},
                {title:'Fec. Nac.', width:'10%', data:'fec_nac'}
            ]
        });
        
        $("#fec_nacimiento", $("#form_poblacion")).on('blur', function(){
            var fec_nac=moment($(this).val());
            var ahora= moment(new Date());
            var anios=meses=0;
            if(fec_nac.isValid()){
                dias_diferencia=ahora.diff(fec_nac, 'days');
                if(dias_diferencia>0){
                    var dif_meses=ahora.diff(fec_nac, 'months');
                    anios=Math.floor((dif_meses/12));//
                    meses=Math.round((dif_meses%12));//
                }
            }
            $("#edad", $("#form_poblacion")).val(anios);
            $("#meses", $("#form_poblacion")).val(meses);
        });

        $("#saved_poblacion").on('click', function(){
            $.ajax('./api/padronhogar.php?oper=guardarpoblacion', {
                method: 'POST',
                dataType:'json',
                data: $("#form_poblacion").serialize(),
                error:function(jqXHR, textStatus, errorThrow){},
                success: function(data, textStatus, jqXHR){
                    var typeNotify=(data.success)?'warning':'error';
                    if(data.success && data.affected_rows==1){
                        typeNotify='success';
                        tabla_poblacion.row.add(data.rows).draw();
                    }
                    toastr[typeNotify](data.messages, 'Sistemas de Encuestas')
                }
            });
        });

        $("#btnregistrar").on('click', function(){
            var dataRequest=$("#formpadronhogar").serialize();
            $.ajax('./api/padronhogar.php?oper=guardar',{
                method: 'POST',
                beforeSend:function(jqXHR, settings){
                    $("#btnregistrar").button('loading');
                    $(":text, select, input[type='numeric'], input[type='date']", $("#formpadronhogar")).attr({disabled:'disabled'});
                },
                data: dataRequest, dataType: 'json',
                error: function(jqXHR, textStatus, errorThrow){
                    toastr.error('Ocurrio un problema al registrar la encuesta', 'Sistema de Encuesta')
                },
                success:function(data, textStatus, jqXHR){
                    var typeNotify=(data.success)?'warning':'error';
                    if(data.success && data.affected_rows==1){
                        typeNotify='success';
                        if(data.id!='' || data.id!=0){
                            $("#id_pgh", $("#form_poblacion")).val(data.id);
                            $('#modal_poblacion').modal({
                                backdrop: 'static',
                                keyboard: false,
                                show: true
                            });
                        }
                    }
                    toastr[typeNotify](data.messages, 'Sistema de Encuestas');
                }, complete:function(){
                    $("#btnregistrar").button('reset');
                    $(":reset", $("#formpadronhogar")).trigger('click');
                    $(":reset", $("#form_poblacion")).trigger('click');
                    $(":text, select, input[type='numeric'], input[type='date']", $("#formpadronhogar")).removeAttr('disabled');
                }
            });
        });
    });
})()