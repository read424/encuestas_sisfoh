<?php
	require_once('conexion/dbsistema.php');
?>
<!DOCTYPE html>
<html lang="es" ng-app="appencuesta">
   <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <title>Sistema de Control de Hogares y Adulto Mayor</title>
      <meta content="Admin Dashboard" name="description" />
      <meta content="Themesbrand" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="assets/images/favicon.ico">
      <script type="text/javascript" language="javascript" src="assets/js/angular.min.js"></script>
      <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
      <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
      <link href="assets/css/toastr.min.css" rel="stylesheet" type="text/css" />
      <!-- Table responsive -->
      <!-- <link href="assets/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css" rel="stylesheet" type="text/css" media="screen"> -->
      <link href="assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
      <link href="assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
      <link href="assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
   <div id="preloader">
      <div id="status">
         <div class="spinner"></div>
      </div>
   </div>
   <header id="topnav">
      <div class="topbar-main">
         <div class="container">
            <div class="logo"> <a href="#" class="logo"> <img src="assets/images/logo-sm.png" alt="" height="30"> </a></div>
            <div class="menu-extras topbar-custom">
              <!--  <div class="search-wrap" id="search-wrap">
                  <div class="search-bar"> <input class="search-input" type="search" placeholder="Search" /> <a href="#" class="close-search toggle-search" data-target="#search-wrap"> <i class="mdi mdi-close-circle"></i> </a></div>
               </div> -->
					
               <ul class="list-inline float-right mb-0">
					<div style="color:RGB(255,255,255); font-size:20px; display:inline; font-weight:bold;">Sistema de Control de Hogares y Adulto Mayor</div>
                  <!-- <li class="list-inline-item dropdown notification-list"> <a class="nav-link waves-effect toggle-search" href="#" data-target="#search-wrap"> <i class="mdi mdi-magnify noti-icon"></i> </a></li> -->
                  <li class="list-inline-item dropdown notification-list hidden-xs-down"> <a class="nav-link waves-effect" href="#" id="btn-fullscreen"> <i class="mdi mdi-fullscreen noti-icon"></i> </a></li>
                  <li class="list-inline-item dropdown notification-list">
                     <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"> <i class="ion-ios7-bell noti-icon"></i> <span class="badge badge-danger noti-icon-badge">3</span> </a>
                     <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                        <div class="dropdown-item noti-title">
                           <h5>Notification (3)</h5>
                        </div>
                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                           <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                           <p class="notify-details"><b>Your order is placed</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                        </a>
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                           <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                           <p class="notify-details"><b>New Message received</b><small class="text-muted">You have 87 unread messages</small></p>
                        </a>
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                           <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                           <p class="notify-details"><b>Your item is shipped</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                        </a>
                        <a href="javascript:void(0);" class="dropdown-item notify-item"> View All </a>
                     </div>
                  </li>
                  <li class="list-inline-item dropdown notification-list">
                     <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"> <img src="assets/images/users/user08.png" alt="user" class="rounded-circle"> </a>
                     <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <a class="dropdown-item" href="#"><i class="dripicons-user text-muted"></i> Perfil</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="cerrar-sesion.php"><i class="dripicons-exit text-muted"></i> Salir</a>
                     </div>
                  </li>
                  <li class="menu-item list-inline-item">
                     <a class="navbar-toggle nav-link">
                        <div class="lines"> <span></span> <span></span> <span></span></div>
                     </a>
                  </li>
               </ul>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
      <div class="navbar-custom">
         <div class="container">
            <div id="navigation">
               <ul class="navigation-menu">
                  <li class="has-submenu">
                     <a href="#"><i class="mdi mdi-view-dashboard"></i>Inicio</a>
                     <!-- <ul class="submenu">
                        <li><a href="principal.php">Inicio</a></li>
                     </ul> -->
                  </li>
                  <li class="has-submenu">
                     <a href="#"><i class="mdi mdi-desktop-mac"></i>Mantenimiento</a>
                     <ul class="submenu">
                        <li><a href="usuario_mostrar.php">Usuarios</a></li>
                     </ul>
                  </li>
                  <li class="has-submenu">
                     <a href="#"><i class="mdi mdi-buffer"></i>Registro de Datos</a>
                     <ul class="submenu megamenu">
                        <li>
                           <ul>
                              <li><a href="fichasocioeconomica_mostrar.php">Ficha SocioEconomica</a></li>
                           </ul>
                        </li>
                        <li style="display:none;">
                           <ul>
                              <li><a href="ui-lightbox.html">Lightbox</a></li>
                              <li><a href="ui-navs.html">Navs</a></li>
                              <li><a href="ui-pagination.html">Pagination</a></li>
                              <li><a href="ui-popover-tooltips.html">Popover & Tooltips</a></li>
                              <li><a href="ui-badge.html">Badge</a></li>
                              <li><a href="ui-carousel.html">Carousel</a></li>
                              <li><a href="ui-video.html">Video</a></li>
                              <li><a href="ui-typography.html">Typography</a></li>
                              <li><a href="ui-sweet-alert.html">Sweet-Alert</a></li>
                           </ul>
                        </li>
                        <li style="display:none;">
                           <ul>
                              <li><a href="ui-grid.html">Grid</a></li>
                              <li><a href="ui-animation.html">Animation</a></li>
                              <li><a href="ui-highlight.html">Highlight</a></li>
                              <li><a href="ui-rating.html">Rating</a></li>
                              <li><a href="ui-nestable.html">Nestable</a></li>
                              <li><a href="ui-alertify.html">Alertify</a></li>
                              <li><a href="ui-rangeslider.html">Range Slider</a></li>
                              <li><a href="ui-sessiontimeout.html">Session Timeout</a></li>
                           </ul>
                        </li>
                     </ul>
                  </li>
                  <li class="has-submenu" style="display:none;">
                     <a href="#"><i class="mdi mdi-cube-outline"></i>Components</a>
                     <ul class="submenu">
                        <li class="has-submenu">
                           <a href="#">Email</a>
                           <ul class="submenu">
                              <li><a href="email-inbox.html">Inbox</a></li>
                              <li><a href="email-read.html">Email Read</a></li>
                              <li><a href="email-compose.html">Email Compose</a></li>
                           </ul>
                        </li>
                        <li> <a href="widgets.html">Widgets</a></li>
                        <li> <a href="calendar.html">Calendar</a></li>
                        <li class="has-submenu">
                           <a href="#">Forms</a>
                           <ul class="submenu">
                              <li><a href="form-elements.html">Form Elements</a></li>
                              <li><a href="form-validation.html">Form Validation</a></li>
                              <li><a href="form-advanced.html">Form Advanced</a></li>
                              <li><a href="form-wizard.html">Form Wizard</a></li>
                              <li><a href="form-editors.html">Form Editors</a></li>
                              <li><a href="form-uploads.html">Form File Upload</a></li>
                              <li><a href="form-mask.html">Form Mask</a></li>
                              <li><a href="form-summernote.html">Summernote</a></li>
                              <li><a href="form-xeditable.html">Form Xeditable</a></li>
                           </ul>
                        </li>
                        <li class="has-submenu">
                           <a href="#">Charts</a>
                           <ul class="submenu">
                              <li><a href="charts-morris.html">Morris Chart</a></li>
                              <li><a href="charts-chartist.html">Chartist Chart</a></li>
                              <li><a href="charts-chartjs.html">Chartjs Chart</a></li>
                              <li><a href="charts-flot.html">Flot Chart</a></li>
                              <li><a href="charts-c3.html">C3 Chart</a></li>
                              <li><a href="charts-sparkline.html">Sparkline Chart</a></li>
                              <li><a href="charts-other.html">Jquery Knob Chart</a></li>
                              <li><a href="charts-peity.html">Peity Chart</a></li>
                           </ul>
                        </li>
                        <li class="has-submenu">
                           <a href="#">Tables</a>
                           <ul class="submenu">
                              <li><a href="tables-basic.html">Basic Tables</a></li>
                              <li><a href="tables-datatable.html">Data Table</a></li>
                              <li><a href="tables-responsive.html">Responsive Table</a></li>
                              <li><a href="tables-editable.html">Editable Table</a></li>
                           </ul>
                        </li>
                        <li class="has-submenu">
                           <a href="#">Icons</a>
                           <ul class="submenu">
                              <li><a href="icons-material.html">Material Design</a></li>
                              <li><a href="icons-ion.html">Ion Icons</a></li>
                              <li><a href="icons-fontawesome.html">Font Awesome</a></li>
                              <li><a href="icons-themify.html">Themify Icons</a></li>
                              <li><a href="icons-dripicons.html">Dripicons</a></li>
                              <li><a href="icons-typicons.html">Typicons Icons</a></li>
                              <li><a href="icons-weather.html">Weather Icons</a></li>
                              <li><a href="icons-mobirise.html">Mobirise Icons</a></li>
                           </ul>
                        </li>
                        <li class="has-submenu">
                           <a href="#">Maps</a>
                           <ul class="submenu">
                              <li><a href="maps-google.html"> Google Map</a></li>
                              <li><a href="maps-vector.html"> Vector Map</a></li>
                           </ul>
                        </li>
                        <li class="has-submenu">
                           <a href="#">Email Templates</a>
                           <ul class="submenu">
                              <li><a href="email-templates-basic.html">Basic Action Email</a></li>
                              <li><a href="email-templates-alert.html">Alert Email</a></li>
                              <li><a href="email-templates-billing.html">Billing Email</a></li>
                           </ul>
                        </li>
                     </ul>
                  </li>
                  <li class="has-submenu" style="display:none;">
                     <a href="#"><i class="mdi mdi-google-pages"></i>Pages</a>
                     <ul class="submenu megamenu">
                        <li>
                           <ul>
                              <li><a href="pages-login.html">Login</a></li>
                              <li><a href="pages-register.html">Register</a></li>
                              <li><a href="pages-recoverpw.html">Recover Password</a></li>
                              <li><a href="pages-lock-screen.html">Lock Screen</a></li>
                              <li><a href="pages-login-2.html">Login 2</a></li>
                              <li><a href="pages-register-2.html">Register 2</a></li>
                              <li><a href="pages-recoverpw-2.html">Recover Password 2</a></li>
                           </ul>
                        </li>
                        <li>
                           <ul>
                              <li><a href="pages-lock-screen-2.html">Lock Screen 2</a></li>
                              <li><a href="pages-timeline.html">Timeline</a></li>
                              <li><a href="pages-invoice.html">Invoice</a></li>
                              <li><a href="pages-directory.html">Directory</a></li>
                              <li><a href="pages-blank.html">Blank Page</a></li>
                              <li><a href="pages-404.html">Error 404</a></li>
                              <li><a href="pages-500.html">Error 500</a></li>
                           </ul>
                        </li>
                        <li>
                           <ul>
                              <li><a href="pages-pricing.html">Pricing</a></li>
                              <li><a href="pages-gallery.html">Gallery</a></li>
                              <li><a href="pages-maintenance.html">Maintenance</a></li>
                              <li><a href="pages-coming-soon.html">Coming Soon</a></li>
                              <li><a href="pages-faq.html">FAQ</a></li>
                              <li><a href="pages-contact.html">Contact</a></li>
                           </ul>
                        </li>
                     </ul>
                  </li>
                  <li class="has-submenu" style="display:none;">
                     <a href="#"><i class="mdi mdi-cart-outline"></i>Ecommerce</a>
                     <ul class="submenu">
                        <li><a href="ecommerce-product-list.html">Product List</a></li>
                        <li><a href="ecommerce-product-grid.html">Product Grid</a></li>
                        <li><a href="ecommerce-order-history.html">Order History</a></li>
                        <li><a href="ecommerce-customers.html">Customers</a></li>
                        <li><a href="ecommerce-product-edit.html">Product Edit</a></li>
                     </ul>
                  </li>
                  <li> <a href="#" target="_blank"><i class="mdi mdi-printer"></i>Reporte</a></li>
               </ul>
            </div>
         </div>
      </div>
   </header>